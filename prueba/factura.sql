-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2020 a las 23:38:32
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `factura`
--
CREATE DATABASE IF NOT EXISTS `factura` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `factura`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `act_f` (IN `id` VARCHAR(7), IN `cliente` INT, IN `feha` DATE, IN `pago` INT)  NO SQL
BEGIN
UPDATE factura SET id_cliente=cliente,fecha=feha,num_pago=pago WHERE  correlativo=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_p` (IN `name_p` VARCHAR(50), IN `precio` DECIMAL(10,2), IN `stock` INT, IN `id_categoria` INT)  NO SQL
BEGIN
INSERT INTO producto(id_producto, name_p, precio, stock, id_categoria) VALUES (null,name_p,precio,stock,id_categoria);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_cliente` (IN `id` INT)  BEGIN
SELECT * FROM cliente WHERE id_cliente= id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_f` (IN `id` VARCHAR(7))  BEGIN
SELECT f.correlativo, c.name,c.lastname, f.fecha, p.nombre FROM factura f INNER JOIN cliente c on c.id_cliente = f.id_cliente INNER JOIN modo_pago p on p.num_pago = f.num_pago WHERE f.correlativo=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_p` (IN `id` INT)  BEGIN
SELECT  p.id_producto,p.name_p,c.name_c,c.descripcion,p.precio,p.stock FROM producto p INNER JOIN categoria c on c.id_categoria = p.id_categoria WHERE p.id_producto = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `categoria` ()  BEGIN
SELECT * FROM categoria;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `d` (IN `co` VARCHAR(7))  BEGIN
DELETE FROM factura WHERE correlativo = co;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_p` (IN `id` INT)  BEGIN
DELETE FROM producto WHERE id_producto = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `d_c` (IN `id` INT)  BEGIN
DELETE FROM cliente WHERE id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editar` (IN `id` INT, IN `name` VARCHAR(50), IN `lastname` VARCHAR(50), IN `direccion` TEXT, IN `fecha_nac` DATE, IN `telefono` INT(8), IN `email` VARCHAR(50))  BEGIN
UPDATE cliente SET name=name,lastname=lastname,direccion=direccion,fecha_nac=fecha_nac,telefono=telefono,email=email WHERE id_cliente=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editar_p` (IN `id` INT, IN `name_p` VARCHAR(50), IN `precio` DECIMAL(10,2), IN `stock` INT, IN `id_categoria` INT)  BEGIN
UPDATE producto SET name_p=name_p,precio=precio,stock=stock,id_categoria=id_categoria WHERE  id_producto=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_c` (IN `name` VARCHAR(50), IN `lastname` VARCHAR(50), IN `direccion` TEXT, IN `fecha_nac` DATE, IN `telefono` INT(8), IN `email` VARCHAR(50))  BEGIN
INSERT INTO cliente(id_cliente, name, lastname, direccion, fecha_nac, telefono, email) VALUES (null,name,lastname,direccion,fecha_nac,telefono,email);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ir_f` (IN `id` INT)  BEGIN
SELECT d.num_detalle,f.correlativo,c.name,c.lastname,c.telefono,f.fecha,pa.nombre,p.name_p,p.precio,d.cantidad,d.precio_d FROM detalle d INNER JOIN factura f on f.correlativo = d.correlativo INNER JOIN producto p on p.id_producto = d.id_producto INNER JOIN cliente c on c.id_cliente = f.id_cliente INNER JOIN modo_pago pa on pa.num_pago = f.num_pago WHERE c.id_cliente = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insercion_factura` (IN `co` VARCHAR(7), IN `clie` INT, IN `fec` DATE, IN `pag` INT)  BEGIN
INSERT INTO factura(correlativo, id_cliente, fecha, num_pago) VALUES (co,clie,fec,pag);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ver_p` ()  BEGIN
SELECT p.id_producto,p.name_p,c.name_c,c.descripcion,p.precio,p.stock FROM producto p INNER JOIN categoria c on c.id_categoria = p.id_categoria;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `view` ()  BEGIN
SELECT f.correlativo, c.name,c.lastname, f.fecha, p.nombre FROM factura f INNER JOIN cliente c on c.id_cliente = f.id_cliente INNER JOIN modo_pago p on p.num_pago = f.num_pago;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `v_cliente` ()  BEGIN
SELECT c.id_cliente, c.name, c.lastname, c.direccion, c.fecha_nac, c.telefono, c.email 
FROM cliente c;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `name_c` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `name_c`, `descripcion`) VALUES
(1, 'domesticos', 'productos para el hogar...'),
(2, 'oficina', 'todo detallado para un buen ambiente laboral'),
(3, 'belleza', 'Desde rizadores hasta no se que cosa para la estetica y belleza de la mujer'),
(4, 'industrial', 'concreto, hierro, laminas, etc.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `direccion` text NOT NULL,
  `fecha_nac` date NOT NULL,
  `telefono` int(8) NOT NULL,
  `email` varchar(60) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `name`, `lastname`, `direccion`, `fecha_nac`, `telefono`, `email`) VALUES
(1, 'Oscar Alberto', 'Delcid Gaitan', 'Colonia Miraflores del Campo Verde, San José Cedros, Los Alpes del New York.', '2013-11-05', 12345678, 'a@gmail.com'),
(2, 'Rosa Flores', 'del Campo Nieve', 'San Salvador', '2014-10-01', 87654321, 'b@gmail.com'),
(28, 'hola', 'mundo', '.php', '2030-01-22', 32543253, 'holamundo@php.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE IF NOT EXISTS `detalle` (
  `num_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `correlativo` varchar(7) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_d` decimal(10,2) NOT NULL,
  PRIMARY KEY (`num_detalle`,`correlativo`),
  KEY `id_producto` (`id_producto`),
  KEY `id_factura` (`correlativo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`num_detalle`, `correlativo`, `id_producto`, `cantidad`, `precio_d`) VALUES
(1, 'PHP0002', 3, 2, '6401.00'),
(2, 'PHP0001', 2, 1, '25.50'),
(3, 'PHP0001', 1, 10, '260.00');

--
-- Disparadores `detalle`
--
DELIMITER $$
CREATE TRIGGER `delete` BEFORE DELETE ON `detalle` FOR EACH ROW BEGIN
INSERT INTO tr_disminuye_stock(num_detalle, hora, accion, correlativo_an, id_producto_an, cantidad_an, precio_an) VALUES (old.num_detalle,now(),'se eliminaron estos registros',old.correlativo,old.id_producto,old.cantidad,old.precio);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert` AFTER INSERT ON `detalle` FOR EACH ROW BEGIN
INSERT INTO tr_disminuye_stock(num_detalle, correlativo, id_producto, cantidad, precio, hora, accion) VALUES (new.num_detalle,new.correlativo,new.id_producto,new.cantidad,new.precio,now(),'estos son los detalles de la factura');
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update` BEFORE UPDATE ON `detalle` FOR EACH ROW BEGIN
INSERT INTO tr_disminuye_stock(num_detalle, correlativo, id_producto, cantidad, precio, hora, accion, correlativo_an, id_producto_an, cantidad_an, precio_an) VALUES (new.num_detalle,new.correlativo,new.id_producto,new.cantidad,new.precio,now(),'estos son los datos de los registros modificados',old.correlativo,old.id_producto,old.cantidad,old.precio);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `correlativo` varchar(7) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `num_pago` int(11) NOT NULL,
  PRIMARY KEY (`correlativo`),
  KEY `id_cliente` (`id_cliente`,`num_pago`),
  KEY `num_pago` (`num_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`correlativo`, `id_cliente`, `fecha`, `num_pago`) VALUES
('PHP0001', 1, '2020-01-21', 2),
('PHP0002', 2, '2020-01-21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modo_pago`
--

CREATE TABLE IF NOT EXISTS `modo_pago` (
  `num_pago` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `otro_detalle` text NOT NULL,
  PRIMARY KEY (`num_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modo_pago`
--

INSERT INTO `modo_pago` (`num_pago`, `nombre`, `otro_detalle`) VALUES
(1, 'tarjeta de credito', 'Banco Agricola, Bancofit, Scotiobank, Azura'),
(2, 'efectivo', 'se admite todo tipo de dinero de dolar estadounidense meno billetes de $500 en adelante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `name_p` varchar(50) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `id_categoria` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `name_p`, `precio`, `stock`, `id_categoria`) VALUES
(1, 'pintacaritas', '26.00', 60, 3),
(2, 'martillo de trueno', '25.50', 5, 4),
(3, 'Laptop iCORE 8th, 1 tb 12gb RAM 8gb ROM ', '3200.50', 990, 2),
(4, 'camisa', '2.50', 23, 1),
(8, 'fdhgxb', '0.75', 121256, 1),
(9, 'fdhgxb', '0.75', 121256, 1),
(10, 'fdhgxb', '0.75', 121256, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tr_disminuye_stock`
--

CREATE TABLE IF NOT EXISTS `tr_disminuye_stock` (
  `num_detalle` int(11) NOT NULL,
  `correlativo` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `hora` datetime NOT NULL,
  `accion` int(11) NOT NULL,
  `correlativo_an` varchar(7) NOT NULL,
  `id_producto_an` int(11) NOT NULL,
  `cantidad_an` int(11) NOT NULL,
  `precio_an` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tr_disminuye_stock`
--

INSERT INTO `tr_disminuye_stock` (`num_detalle`, `correlativo`, `id_producto`, `cantidad`, `precio`, `hora`, `accion`, `correlativo_an`, `id_producto_an`, `cantidad_an`, `precio_an`) VALUES
(3, 'PHP0001', 1, 12, '312.00', '2020-01-21 11:24:21', 0, '', 0, 0, '0.00'),
(0, 'PHP0001', 2, 45, '45.00', '2020-01-21 11:25:13', 0, '', 0, 0, '0.00'),
(5, 'PHP0002', 3, 2, '3.00', '2020-01-21 11:26:31', 0, '', 0, 0, '0.00'),
(5, '', 0, 0, '0.00', '2020-01-21 11:26:48', 0, 'PHP0002', 3, 2, '3.00'),
(4, '', 0, 0, '0.00', '2020-01-21 11:27:07', 0, 'PHP0001', 2, 45, '45.00'),
(3, 'PHP0001', 1, 10, '260.00', '2020-01-21 11:27:41', 0, 'PHP0001', 1, 12, '312.00');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`correlativo`) REFERENCES `factura` (`correlativo`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`num_pago`) REFERENCES `modo_pago` (`num_pago`),
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
