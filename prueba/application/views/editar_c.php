<body>
	<div class="container">
		
			<form action="<?php echo base_url('cliente_c/ingresar') ?>" method="POST">
			<div class="row">
				<br>
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el nombre del cliente:
				</div>
				<div class="col-md-3">
					<input type="text" name="name" id="name" class="form-control">
					<input type="hidden" name="id" id="id"> 
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el apellido del cliente:
				</div>
				<div class="col-md-3">
					<input type="text" name="lastname" id="lastname" class="form-control">
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese la direccion del cliente:
				</div>
				<div class="col-md-3">
					<textarea name="direccion" id="direccion" class="form-control" cols="25" rows="5"></textarea>
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese la fecha de nacimiento del cliente:
				</div>
				<div class="col-md-3">
					<input type="date" name="fecha_nac" id="fecha_nac" class="form-control" >
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el numero de telefono del cliente:
				</div>
				<div class="col-md-3">
					<input type="text" name="telefono" id="telefon" onkeypress="if(isNaN(String.fromCharCode(event.keyCode))) return false;" class="form-control" maxlength="8" minlength="8" >
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el correo electronico del cliente:
				</div>
				<div class="col-md-3">
					<input type="text" name="email" id="email" class="form-control">
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-outline-primary">Guardar</button>
				</div>
				<div class="col-md-3">
					<a href="<?php echo base_url() ?>" class="btn btn-outline-secondary">Cancelar</a>
				</div>
				<div class="col-md-3"></div>
			</div>
			
        
		</form>
		
	</div>