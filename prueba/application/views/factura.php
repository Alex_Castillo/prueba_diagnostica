<body>
	<div class="container">
		<br>
		<h1 class="text-center bg-dark text-white">DETALLE DE FACTURA</h1>
		<br>
		<br>
		<br>
		<table class="table table-hover table-striped table-sm table-bordered">
			<thead class="thead-dark text-center">
				<th>N°</th>
				<th>Correlativo</th>
				<th>Nombre del cliente</th>
				<th>Fecha de facturacion</th>
				<th>Modo de pago</th>>
			</thead>
			<tbody class="text-center">
				<?php
				$n=1;
				 foreach ($factura as $f): ?>
					<tr>
						<td><?php echo $n; $n++; ?></td>
						<td><?php echo $f->correlativo ?></td>
						<td><?php echo $f->name.' '.$f->lastname ?></td>
						<td><?php echo $f->fecha ?></td>
						<td><?php echo $f->nombre ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>