<body>
	<div class="container">
		<br>
		<h1 class="text-center bg-dark text-white">Datos de clientes</h1>
		<br>
			<a href="<?php echo base_url('cliente_c/ir') ?>" class="btn btn-sm btn-outline-success">Ingresar nuevo cliente</a>
		<br>
		<br>
		<table class="table table-hover table-striped table-sm table-bordered">
			<thead class="thead-dark text-center">
				<th>N°</th>
				<th>Nombre del cliente</th>
				<th>Direccion del cliente</th>
				<th>Fecha de nacimiento</th>
				<th>Telefono</th>
				<th>Correo electronico</th>
				<th>Factura</th>
				<th>Actualizar</th>
				<th>Eliminar</th>
			</thead>
			<tbody class="text-center">
				<?php
				$n=1;
				 foreach ($cliente as $c): ?>
					<tr>
						<td><?php echo $n; $n++; ?></td>
						<td><?php echo $c->name.' '.$c->lastname ?></td>
						<td><?php echo $c->direccion ?></td>
						<td><?php echo $c->fecha_nac ?></td>
						<td><?php echo $c->telefono ?></td>
						<td><?php echo $c->email ?></td>
						<td><a href="<?php echo base_url('cliente_c/ir_f/').$c->id_cliente ?>" class="btn btn-sm btn-outline-info">factura</a></td>
						<td><a href="<?php echo base_url('cliente_c/c_id/').$c->id_cliente ?>" class="btn btn-sm btn-outline-success">editar</a></td>
						<td><a href="<?php echo base_url('cliente_c/eliminar/').$c->id_cliente ?>" onclick="return confirm('estas seguro de eliminar este cliente?') " class="btn btn-sm btn-outline-danger">eliminar</a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>