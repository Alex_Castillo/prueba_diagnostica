<body>
	<div class="container">
		
			<form action="<?php echo base_url('producto_c/ingresar') ?>" method="POST">
			<div class="row">
				<br>
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el nombre del producto:
				</div>
				<div class="col-md-3">
					<input type="text" name="name_p" id="name_p" class="form-control"> 
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el precio:
				</div>
				<div class="col-md-3">
					<input type="text" name="precio" id="precio" class="form-control">
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Ingrese el Stock:
				</div>
				<div class="col-md-3">
					<input type="text" name="stock" id="stock" class="form-control" onkeypress="if(isNaN(String.fromCharCode(event.keyCode))) return false;" class="form-control" maxlength="8" minlength="8">
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					Seleccione la categoria del producto:
				</div>
				<div class="col-md-3">
					<select name="id_categoria" id="id_categoria" class="form-control">
						<option value="">
							Seleccione una opcion
						</option>
						<?php foreach ($categoria as $c): ?>
							<option value="<?php echo $c->id_categoria ?>">
								<?php echo $c->name_c ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-outline-primary">Guardar</button>
				</div>
				<div class="col-md-3">
					<a href="<?php echo base_url('producto_c') ?>" class="btn btn-outline-secondary">Cancelar</a>
				</div>
				<div class="col-md-3"></div>
			</div>
			
        
		</form>
		
	</div>