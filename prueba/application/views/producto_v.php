<body>
	<div class="container">
		<br>
		<h1 class="text-center bg-dark text-white">PRODUCTOS</h1>
		<br>
			<a href="<?php echo base_url('producto_c/ir') ?>" class="btn btn-sm btn-outline-success">Ingresar nuevo producto</a>
		<br>
		<br>
		<table class="table table-hover table-striped table-sm table-bordered">
			<thead class="thead-dark text-center">
				<th>N°</th>
				<th>Nombre del producto</th>
				<th>Precio</th>
				<th>Stock</th>
				<th>Categoria</th>
				<th>Actualizar</th>
				<th>Eliminar</th>
			</thead>
			<tbody class="text-center">
				<?php
				$n=1;
				 foreach ($producto as $c): ?>
					<tr>
						<td><?php echo $n; $n++; ?></td>
						<td><?php echo $c->name_p ?></td>
						<td><?php echo $c->precio ?></td>
						<td><?php echo $c->stock ?></td>
						<td><?php echo $c->name_c ?></td>
						<td><a href="<?php echo base_url('producto_c/c_id/').$c->id_producto ?>" class="btn btn-sm btn-outline-success">editar</a></td>
						<td><a href="<?php echo base_url('producto_c/eliminar/').$c->id_producto ?>" onclick="return confirm('estas seguro de eliminar este cliente?') " class="btn btn-sm btn-outline-danger">eliminar</a></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>