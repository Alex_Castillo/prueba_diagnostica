<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cliente_m extends CI_Model {

	public function mostrar(){
		$q = 'CALL v_cliente()';
		$r = $this->db->query($q);
		return $r->result();
	}
	public function delete($id){
		$q = 'CALL d_c(?)';
		$r = $this->db->query($q,$id);
		if($this->db->affected_rows() >0){
			return "d";
		}else{
			return false;
		}
	}
	public function insertar($d){
		$q = 'CALL insertar_c(?,?,?,?,?,?)';
		$this->db->query($q,$d);
	}
	public function c_id($id){
		$q = 'CALL cargar_cliente(?)';
		$r = $this->db->query($q,$id);
		return $r->result();
	}
	public function editar($d){
		$q = 'CALL editar(?,?,?,?,?,?,?)';
		$this->db->query($q,$d);
	}
	public function ir_f($id){
		$q = 'CALL ir_f(?)';
		$r = $this->db->query($q,$id);
		return $r->result();
	}
}