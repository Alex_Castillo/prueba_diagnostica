<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class factura_m extends CI_Model {

	public function mostrar(){
		$q = 'CALL view()';
		$r = $this->db->query($q);
		return $r->result();
	}
	public function delete($id){
		$q = 'CALL d(?)';
		$r = $this->db->query($q,$id);
		if($this->db->affected_rows() >0){
			return "d";
		}else{
			return false;
		}
	}
	public function insertar($d){
		$q = 'CALL sp_insercion_factura(?,?,?,?)';
		$this->db->query($q,$d);
	}
	public function c_id($id){
		$q = 'CALL cargar_f(?)';
		$r = $this->db->query($q,$id);
		return $r->result();
	}
	public function editar($d){
		$q = 'CALL act_f(?,?,?,?)';
		$this->db->query($q,$d);
	}
	
}