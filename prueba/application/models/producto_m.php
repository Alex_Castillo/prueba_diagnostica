<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_m extends CI_Model {

	public function mostrar(){
		$q = 'CALL ver_p()';
		$r = $this->db->query($q);
		return $r->result();
	}
	public function mostrar_c(){
		$q = 'CALL categoria()';
		$r = $this->db->query($q);
		return $r->result();
	}
	public function delete($id){
		$q = 'CALL delete_p(?)';
		$r = $this->db->query($q,$id);
		if($this->db->affected_rows() >0){
			return "d";
		}else{
			return false;
		}
	}
	public function insertar($d){
		$q = 'CALL add_p(?,?,?,?)';
		$this->db->query($q,$d);
	}
	public function c_id($id){
		$q = 'CALL cargar_p(?)';
		$r = $this->db->query($q,$id);
		return $r->result();
	}
	public function editar($d){
		$q = 'CALL editar_p(?,?,?,?,?)';
		$this->db->query($q,$d);
	}
}