<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class factura_c extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('factura_m');
	}
	public function index(){
		$d = array(
			'title' => 'datos de factura',
			'hola' => 'DATOS DE FACTURA',
			'factura' => $this->factura_m->mostrar()
		);
		$this->load->view('template/header',$d);
		$this->load->view('factura');
		$this->load->view('template/footer');
	}
	// eliminar
	public function eliminar($id){
		$e = $this->factura_m->delete($id);
		if($e == 'd'){
			echo "<script>alert('eliminado')</script>";
		}else if($e == false){
			echo "<script>alert('no se puede eliminar')</script>";
		}
		redirect('factura_c/index','refresh');
	}
	// Vista para insertar
	public function ir(){
		$d = array(
			'title' => 'ingresar datos de factura',
			'hola' => 'INSERTAR DATOS DE FACTURA',
			'cliente' => $this->factura_m->mostrar_c(),
			'pago' => $this->factura_m->mostrar_p()
		);
		$this->load->view('template/header',$d);
		$this->load->view('ingresar_f');
		$this->load->view('template/footer');
	}
	// funcion insertar
	public function ingresar(){
		$d = array(
			'name_p' => $this->input->post('name_p'),
			'precio' => $this->input->post('precio'),
			'stock' => $this->input->post('stock'),
			'id_categoria' => $this->input->post('id_categoria')
		);
		$this->factura_m->insertar($d);
		redirect('factura_c/index','refresh');
	}
	// vista para actualizar
	public function c_id($id){
		$d = array(
			'title' => 'editar datos de factura',
			'hola' => 'EDITAR DATOS DE FACTURA',
			'factura' => $this->factura_m->c_id($id),
			'cliente' => $this->factura_m->mostrar_c(),
			'pago' => $this->factura_m->mostrar_p()
		);
		$this->load->view('template/header',$d);
		$this->load->view('editar_f');
		$this->load->view('template/footer');
	}
	public function actualizar(){
		$d = array(
			'id' => $this->input->post('id'),
			'name_p' => $this->input->post('name_p'),
			'precio' => $this->input->post('precio'),
			'stock' => $this->input->post('stock'),
			'id_categoria' => $this->input->post('id_categoria')
		);
		$this->factura_m->editar($d);
		redirect('factura_c/index','refresh');
	}
	
}
