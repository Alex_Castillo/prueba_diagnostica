<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class producto_c extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('producto_m');
	}
	public function index(){
		$d = array(
			'title' => 'datos de productos',
			'hola' => 'DATOS DE PRODUCTOS',
			'producto' => $this->producto_m->mostrar()
		);
		$this->load->view('template/header',$d);
		$this->load->view('producto_v');
		$this->load->view('template/footer');
	}
	// eliminar
	public function eliminar($id){
		$e = $this->producto_m->delete($id);
		if($e == 'd'){
			echo "<script>alert('eliminado')</script>";
		}else if($e == false){
			echo "<script>alert('no se puede eliminar')</script>";
		}
		redirect('producto_c/index','refresh');
	}
	// Vista para insertar
	public function ir(){
		$d = array(
			'title' => 'ingresar datos de producto',
			'hola' => 'INSERTAR DATOS DE PRODUCTOS',
			'categoria' => $this->producto_m->mostrar_c()
		);
		$this->load->view('template/header',$d);
		$this->load->view('ingresar_p');
		$this->load->view('template/footer');
	}
	// funcion insertar
	public function ingresar(){
		$d = array(
			'name_p' => $this->input->post('name_p'),
			'precio' => $this->input->post('precio'),
			'stock' => $this->input->post('stock'),
			'id_categoria' => $this->input->post('id_categoria')
		);
		$this->producto_m->insertar($d);
		redirect('producto_c/index','refresh');
	}
	// vista para actualizar
	public function c_id($id){
		$d = array(
			'title' => 'editar datos de productos',
			'hola' => 'EDITAR DATOS DE PRODUCTOS',
			'producto' => $this->producto_m->c_id($id),
			'categoria' => $this->producto_m->mostrar_c()
		);
		$this->load->view('template/header',$d);
		$this->load->view('editar_p');
		$this->load->view('template/footer');
	}
	public function actualizar(){
		$d = array(
			'id' => $this->input->post('id'),
			'name_p' => $this->input->post('name_p'),
			'precio' => $this->input->post('precio'),
			'stock' => $this->input->post('stock'),
			'id_categoria' => $this->input->post('id_categoria')
		);
		$this->producto_m->editar($d);
		redirect('producto_c/index','refresh');
	}
	
}
