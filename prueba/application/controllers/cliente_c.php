<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cliente_c extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('cliente_m');
	}
	public function index(){
		$d = array(
			'title' => 'datos de cliente',
			'hola' => 'DATOS DE CLIENTES',
			'cliente' => $this->cliente_m->mostrar()
		);
		$this->load->view('template/header',$d);
		$this->load->view('cliente_v');
		$this->load->view('template/footer');
	}
	// eliminar
	public function eliminar($id){
		$e = $this->cliente_m->delete($id);
		if($e == 'd'){
			echo "<script>alert('eliminado')</script>";
		}else if($e == false){
			echo "<script>alert('no se puede eliminar')</script>";
		}
		redirect('cliente_c/index','refresh');
	}
	// Vista para insertar
	public function ir(){
		$d = array(
			'title' => 'ingresar datos de cliente',
			'hola' => 'INSERTAR DATOS DE CLIENTES'
		);
		$this->load->view('template/header',$d);
		$this->load->view('editar_c');
		$this->load->view('template/footer');
	}
	// funcion insertar
	public function ingresar(){
		$d = array(
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'direccion' => $this->input->post('direccion'),
			'fecha_nac' => $this->input->post('fecha_nac'),
			'telefono' => $this->input->post('telefono'),
			'email' => $this->input->post('email')
		);
		$this->cliente_m->insertar($d);
		redirect('cliente_c/index','refresh');
	}
	// vista para actualizar
	public function c_id($id){
		$d = array(
			'title' => 'editar datos de cliente',
			'hola' => 'EDITAR DATOS DE CLIENTES',
			'cliente' => $this->cliente_m->c_id($id)
		);
		$this->load->view('template/header',$d);
		$this->load->view('ingresar');
		$this->load->view('template/footer');
	}
	public function actualizar(){
		$d = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'direccion' => $this->input->post('direccion'),
			'fecha_nac' => $this->input->post('fecha_nac'),
			'telefono' => $this->input->post('telefono'),
			'email' => $this->input->post('email')
		);
		$this->cliente_m->editar($d);
		redirect('cliente_c/index','refresh');
	}
	// funcion ir a vista de farctura
	public function ir_f($id){
		$d = array(
			'title' => 'ingresar datos de cliente',
			'hola' => 'DATOS DE FACTURACION DE CLIENTES',
			'factura' => $this->cliente_m->ir_f($id)
		);
		$this->load->view('template/header',$d);
		$this->load->view('factura');
		$this->load->view('template/footer');
	}
}
